##--------- Generic stuff all our Dockerfiles should start with so we get caching ------------
#FROM debian:stretch as stable
#MAINTAINER Tim Sutton<tim@kartoza.com>
#
#RUN  export DEBIAN_FRONTEND=noninteractive
#ENV  DEBIAN_FRONTEND noninteractive
#RUN  dpkg-divert --local --rename --add /sbin/initctl
#
#
#RUN apt-get update \
# && apt-get install --no-install-recommends apt-utils -y \
# && apt-get install --no-install-recommends wget  libsqlite3-dev sqlite3 gcc cc1111 tcc clang bcc flex bison autoconf automake zlib1g-dev bzip2 libjpeg-dev ncurses-dev libpng-dev libtiff5-dev libfreetype6-dev libpam0g-dev libssl-dev libxml2-dev gettext  libpcre++-dev pkg-config bash-completion -y \
# && apt-get install --no-install-recommends build-essential -y \
# && wget http://download.osgeo.org/proj/proj-5.2.0.tar.gz \
# && mkdir gdal \
# && tar -zxf proj-5.2.0.tar.gz -C gdal/ \
# && cd gdal/proj-5.2.0/ \
# && ./configure \
# && make && make install \
# && export LD_LIBRARY_PATH=/lib:/usr/lib:/usr/local/lib
#
#
## ---------------- GDAL
#
#RUN apt-get install --no-install-recommends -y build-essential wget pkg-config bash-completion apt-utils
#
#
##libkml Support
###---------This part is depending on prebuilt libraries. In the future we may replace this by compiling them from source.
#RUN wget http://s3.amazonaws.com/etc-data.koordinates.com/gdal-travisci/install-libkml-r864-64bit.tar.gz
#RUN tar xzf install-libkml-r864-64bit.tar.gz
#
##Copy these required files to  /usr/local
#RUN cp -r install-libkml/include/* /usr/local/include
#RUN cp -r install-libkml/lib/* /usr/local/lib
#RUN ldconfig
#
#
#RUN apt-get remove libgdal-dev
#
##download GDAL
#RUN pwd
#
#RUN wget http://download.osgeo.org/gdal/2.4.2/gdal-2.4.2.tar.gz \
# && tar xzf gdal-2.4.2.tar.gz \
# && cd gdal-2.4.2 \
# && ./configure --with-libkml \
# && make \
# && make install
#
##Build and install the Debian package:
#
##sudo apt-get install checkinstall
##sudo checkinstall
##-------------------
#
#RUN wget http://download.osgeo.org/geos/geos-3.7.2.tar.bz2 \
# && apt-get install --no-install-recommends bzip2 apt-utils -y \
# && tar xjf geos-3.7.2.tar.bz2 \
# && cd geos-3.7.2 \
# && ./configure \
# && make \
# && make install
#
#
#
#RUN apt-get -y update; apt-get -y install gnupg2 wget ca-certificates rpl pwgen
#RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
#RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
#
##-------------Application Specific Stuff ----------------------------------------------------
#
#
#
## We add postgis as well to prevent build errors (that we dont see on local builds)
## on docker hub e.g.
## The following packages have unmet dependencies:
#RUN apt-get update; apt-get install --no-install-recommends -y \
#     postgresql-client-10 \
#     postgresql-common \
#     postgresql-10 \
#     postgresql-10-postgis-2.5 \
#     postgresql-10-postgis-2.5-scripts \
#     postgresql-10-pgrouting \
#     postgresql-contrib-10 \
#     netcat \
#     tzdata \
#     ntp
#
#ENV TZ America/Sao_Paulo
#
## Open port 5432 so linked containers can see them
#EXPOSE 5432
#
## Run any additional tasks here that are too tedious to put in
## this dockerfile directly.
#ADD env-data.sh /env-data.sh
#ADD setup.sh /setup.sh
#RUN chmod +x /setup.sh
#RUN /setup.sh
#
## We will run any commands in this when the container starts
#ADD docker-entrypoint.sh /docker-entrypoint.sh
#ADD setup-conf.sh /
#ADD setup-database.sh /
#ADD setup-pg_hba.sh /
#ADD setup-replication.sh /
#ADD setup-ssl.sh /
#ADD setup-user.sh /
#ADD postgresql.conf /tmp/postgresql.conf
#RUN chmod +x /docker-entrypoint.sh
#
## criar diretorio para os indices / SSD conforme conf da infra
#RUN mkdir /index_ssd && chown -R postgres:postgres /index_ssd
#RUN mkdir /geo && chown -R postgres:postgres /geo
#
## Optimise postgresql
#RUN echo "kernel.shmmax=543252480" >> /etc/sysctl.conf
#RUN echo "kernel.shmall=2097152" >> /etc/sysctl.conf
#
#ENTRYPOINT /docker-entrypoint.sh

ARG CI_COMMIT_REF_SLUG
FROM registry.gitlab.com/pdci/postgis/base:${CI_COMMIT_REF_SLUG} AS stable
#FROM kartoza/postgis:11.0-2.5 as stable

ENV TZ America/Sao_Paulo

#RUN sed -i '/172.0.0.0\/8/d' /etc/postgresql/11/main/pg_hba.conf
#RUN sed -i '/192.168.0.0\/16/d' /etc/postgresql/11/main/pg_hba.conf
#
# Run any additional tasks here that are too tedious to put in
# this dockerfile directly.
#ADD env-data.sh /env-data.sh
#ADD setup.sh /setup.sh
#RUN chmod +x /setup.sh
#RUN /setup.sh


# We will run any commands in this when the container starts
#ADD docker-entrypoint.sh /docker-entrypoint.sh
#ADD setup-conf.sh /
#ADD setup-database.sh /
#ADD setup-pg_hba.sh /
#ADD setup-replication.sh /
#ADD setup-ssl.sh /
#ADD setup-user.sh /
#RUN chmod +x /docker-entrypoint.sh

ADD pg_hba.conf.template /etc/postgresql/11/main/pg_hba.conf.template

ADD docker-entrypoint-initdb.d/ /docker-entrypoint-initdb.d/
# criar diretorio para os indices / SSD conforme conf da infra
RUN mkdir -p /index_ssd && \
    chown -R postgres:postgres /index_ssd && \
    mkdir -p /geo && \
    chown -R postgres:postgres /geo  && \
    chmod +x /scripts/docker-entrypoint.sh

ENTRYPOINT /scripts/docker-entrypoint.sh

#########################################################


FROM stable as develop
ADD docker-entrypoint-initdb.d/ /docker-entrypoint-initdb.d/

ENTRYPOINT /docker-entrypoint.sh
