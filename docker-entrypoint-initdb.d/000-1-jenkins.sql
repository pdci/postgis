
DO
$do$
    BEGIN
            IF NOT EXISTS (
                    SELECT                       -- SELECT list can stay empty for this
                    FROM   pg_catalog.pg_roles
                    WHERE  rolname = 'usr_jenkins') THEN

                create user usr_jenkins with password 'usr_jenkins';
        end if;
    END;
$do$;