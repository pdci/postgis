
DO
$do$
    BEGIN

            CREATE SEQUENCE IF NOT EXISTS jenkins.controle_versao_ordem_seq
                INCREMENT 1
                START 1
                MINVALUE 1
                MAXVALUE 2147483647
                CACHE 1;

            ALTER SEQUENCE jenkins.controle_versao_ordem_seq
                OWNER TO usr_jenkins;

            GRANT ALL ON SEQUENCE jenkins.controle_versao_ordem_seq TO usr_jenkins WITH GRANT OPTION;

            -- Table: jenkins.controle_versao

            -- DROP TABLE jenkins.controle_versao;

            CREATE TABLE jenkins.controle_versao
            (
                des_tag text COLLATE pg_catalog."default" NOT NULL,
                script_rodado text COLLATE pg_catalog."default" NOT NULL,
                data_inclusao text COLLATE pg_catalog."default" NOT NULL DEFAULT now(),
                ordem integer NOT NULL DEFAULT nextval('jenkins.controle_versao_ordem_seq'::regclass),
                sistema text COLLATE pg_catalog."default" NOT NULL,
                username text COLLATE pg_catalog."default" NOT NULL,
                ticket text COLLATE pg_catalog."default",
                obs text COLLATE pg_catalog."default",
                CONSTRAINT controle_versao_pk PRIMARY KEY (sistema, script_rodado)
            );

            ALTER TABLE jenkins.controle_versao
                OWNER to usr_jenkins;

            GRANT ALL ON TABLE jenkins.controle_versao TO usr_jenkins WITH GRANT OPTION;

            COMMENT ON TABLE jenkins.controle_versao
                IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado';

            COMMENT ON COLUMN jenkins.controle_versao.sistema
                IS 'tabela com o nome do projeto';

            COMMENT ON COLUMN jenkins.controle_versao.username
                IS 'Usuário logado no  jenkins que fez o deploy no banco de dados';

            COMMENT ON COLUMN jenkins.controle_versao.ticket
                IS 'Número do chamado da infra';
            -- Index: controle_versao_pk_i

            -- DROP INDEX jenkins.controle_versao_pk_i;

            CREATE INDEX IF NOT EXISTS controle_versao_pk_i
                ON jenkins.controle_versao USING btree
                    (sistema COLLATE pg_catalog."default" ASC NULLS LAST, script_rodado COLLATE pg_catalog."default" ASC NULLS LAST)
                TABLESPACE pg_default;

            grant all on all tables in schema jenkins to usr_jenkins;
            grant usage on all sequences in schema jenkins to usr_jenkins;
            grant execute on all functions in schema jenkins to usr_jenkins;
    END;
$do$;