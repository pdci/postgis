
DO
$do$
    BEGIN
            IF NOT EXISTS (
                    select  -- SELECT list can stay empty for this
                    from information_schema.schemata  where schema_name = 'jenkins'
                ) THEN

            CREATE SCHEMA IF NOT EXISTS jenkins  AUTHORIZATION usr_jenkins;



            COMMENT ON SCHEMA jenkins
                IS 'SCHEMA responsavel em controlar as atualizações de sql no banco de dados do ambiente atraves do deploy do jenkins';

            GRANT ALL ON SCHEMA jenkins TO usr_jenkins WITH GRANT OPTION;
            END IF;
    END;
$do$;